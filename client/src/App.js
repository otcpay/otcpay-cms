import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import LoadingOverlay from 'react-loading-overlay';
import { w3cwebsocket as W3CWebSocket } from 'websocket';

import './App.css';

import { toggle } from './redux/actions';
import {
  SET_LOADING, SET_LOGGEDIN, SET_ALERT, SET_VISIBILITY, SET_NOTIFICATION,
} from './redux/actionTypes';
import { headers, tokenKey, roleKey } from './util/common';
import Login from './component/Login';
import Dashboard from './component/Dashboard';

function WebSocket(callback) {
  try {
    const client = new W3CWebSocket('wss://otcpay-cms.herokuapp.com');
    client.onopen = () => {
      console.log('WebSocket Client Connected');
    };
    client.onmessage = (message) => {
      try {
        if (message.data !== 'ok' && client.readyState === client.OPEN) {
          callback(message.data, SET_NOTIFICATION);
        } else {
          console.log(message);
        }
      } catch (e) {
        console.error('failed to send message', e);
      }
    };
    client.onclose = () => {
      try {
        setTimeout(() => {
          WebSocket();
        }, 5000);
      } catch(e) {
        console.error('failed to reset connection', e);
      }
    };
    client.onerror = () => {
      console.error('Connection Error');
    };
  } catch (e) {
    console.error('Failed to start websocket connection', e);
  }
}

function App(props) {
  const {
    isLoading, toggleLoading, isActive, toggleActive,
    alert, toggleAlert, toggleItem, env, toggleEnv,
    toggleVisibility, notification, toggleNotification,
  } = props;

  useEffect(() => {
    WebSocket(toggleNotification);

    toggleLoading(true, SET_LOADING);
    const jwttoken = localStorage.getItem(tokenKey) || '';
    const role = localStorage.getItem(roleKey) || '';
    if (jwttoken !== '') {
      fetch(`${env}/validate`, {
        headers,
        method: 'POST',
        body: JSON.stringify({ jwttoken }),
      }).then((response) => {
        toggleLoading(false, SET_LOADING);
        if (response.ok) {
          toggleActive(true, SET_LOGGEDIN);

          if (role !== '') {
            fetch(`${env}/authorization/groups/${role}`, {
              headers: { ...headers, Authorization: `Bearer ${jwttoken}` },
              method: 'GET',
            }).then((rsp) => {
              if (!rsp.ok) {
                throw new Error('Failed to get authorization group');
              }

              return rsp.json();
            }).then((authData) => {
              toggleVisibility(authData.permission, SET_VISIBILITY);
            });
          }
        } else {
          toggleActive(false, SET_LOGGEDIN);
          toggleAlert('Token invalid', SET_ALERT);
        }
      }).catch((e) => {
        toggleLoading(false, SET_LOADING);
        toggleAlert(e.message, SET_ALERT);
        toggleActive(false, SET_LOGGEDIN);
      });
    } else {
      toggleLoading(false, SET_LOADING);
    }
  }, []);

  // eslint-disable-next-line no-unused-vars
  const handleAlertClose = (event, reason) => {
    toggleAlert('', SET_ALERT);
  };

  // eslint-disable-next-line no-unused-vars
  const handleNotificationClose = (event, reason) => {
    toggleNotification('', SET_NOTIFICATION);
  };

  return (
    <LoadingOverlay
      active={isLoading}
      spinner
      text='Loading...'
    >
      <Snackbar
        open={alert !== ''}
        autoHideDuration={3000}
        onClose={handleAlertClose}
        message={alert}
        key='alert'
      />

      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={notification !== ''}
        autoHideDuration={5000}
        onClose={handleNotificationClose}
        message={notification}
        key='notification'
      />
      <div className='App'>
        {
          isActive ? (
            <Dashboard
              env={env}
              toggleActive={toggleActive}
              toggleAlert={toggleAlert}
              toggleItem={toggleItem}
            />
          ) : (
            <Login
              env={env}
              toggleVisibility={toggleVisibility}
              toggleEnv={toggleEnv}
              toggleLoading={toggleLoading}
              toggleActive={toggleActive}
              toggleAlert={toggleAlert}
            />
          )
        }
      </div>
    </LoadingOverlay>
  );
}

const mapStateToProps = (state) => ({
  notification: state.general.notification,
  isLoading: state.general.loading,
  isActive: state.general.loggedIn,
  alert: state.general.alert,
  env: state.general.env,
});

const mapDispatchToProps = {
  toggleNotification: toggle,
  toggleVisibility: toggle,
  toggleLoading: toggle,
  toggleActive: toggle,
  toggleAlert: toggle,
  toggleItem: toggle,
  toggleEnv: toggle,
};

App.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isActive: PropTypes.bool.isRequired,
  env: PropTypes.string.isRequired,
  alert: PropTypes.string.isRequired,
  notification: PropTypes.string.isRequired,
  toggleEnv: PropTypes.func.isRequired,
  toggleItem: PropTypes.func.isRequired,
  toggleAlert: PropTypes.func.isRequired,
  toggleActive: PropTypes.func.isRequired,
  toggleLoading: PropTypes.func.isRequired,
  toggleVisibility: PropTypes.func.isRequired,
  toggleNotification: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
