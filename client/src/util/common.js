import React from 'react';

import PeopleIcon from '@material-ui/icons/People';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import ListIcon from '@material-ui/icons/List';
import PaymentIcon from '@material-ui/icons/Payment';
import ReceiptIcon from '@material-ui/icons/Receipt';
import SettingsIcon from '@material-ui/icons/Settings';
import DeveloperBoardIcon from '@material-ui/icons/DeveloperBoard';
import EqualizerIcon from '@material-ui/icons/Equalizer';

const headers = { 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json' };
const tokenKey = 'sessionToken';
const roleKey = 'role';

const disabledFields = [
  'id', 'userId', 'invoiceId', 'adId', 'createdDate', 'updatedDate', 'settledTime', 'idProof', 'addressProof',
  'spreading', 'externalRate', 'wallets',
];

const onCreateFields = [
  'userId', 'invoiceId', 'adId', 'settledTime',
];

const singleImgFields = [
  'profilePic', 'invoiceImage', 'bankImage',
];

const dateFields = [
  'createdDate', 'updatedDate', 'settledTime',
];

const booleanFields = [
  'verified', 'settled',
];

const searchableObjects = [
  'users', 'advertisment', 'buySellRecords', 'settlements', 'invoicePayments',
];

const hiddenFields = {
  users: [
    'googleId', 'whatsappId', 'wechatId', 'telegramId', 'username', 'account', 'idProof', 'addressProof', 'wallets',
  ],
  advertisements: [
    'profilePic',
  ],
  buySellRecords: [],
  invoicePayments: [],
  settlements: [],
};

const additionalFields = {
  users: [
    {
      index: 5,
      key: 'verification',
    },
    {
      index: 4,
      key: 'contact',
    },
    {
      index: 3,
      key: 'user info',
    },
  ],
  advertisements: [],
  buySellRecords: [],
  invoicePayments: [],
  settlements: [
    {
      index: 4,
      key: 'profit',
    },
    {
      index: 5,
      key: 'profit (HKD)',
    },
  ],
};

const objectFields = {
  users: [
    'id',
    'account',
    'username',
    'type',
    'verified',
    'approvedBy',
    'whatsappId',
    'wechatId',
    'telegramId',
    'googleId',
    'password',
    'profilePic',
    'region',
    'idProof',
    'addressProof',
    'wallets',
    'createdDate',
    'updatedDate',
  ],
  advertisements: [
    'id',
    'profilePic',
    'type',
    'status',
    'crytocurrency',
    'upperLimit',
    'lowerLimit',
    'settleCurrency',
    'offer',
    'name',
    'region',
    'contact',
    'userId',
    'createdDate',
    'updatedDate',
  ],
  buySellRecords: [
    'id',
    'settled',
    'settleRate',
    'type',
    'status',
    'crytocurrency',
    'settleCurrency',
    'amount',
    'offerRate',
    'totalCost',
    'name',
    'contact',
    'userId',
    'adId',
    'createdDate',
    'updatedDate',
  ],
  invoicePayments: [
    'id',
    'type',
    'status',
    'receivedAmount',
    'sendFrom',
    'sendTo',
    'receivedCurrency',
    'sendCurrency',
    'amountNeeded',
    'amountSaved',
    'referenceRate',
    'receiveName',
    'receiveBank',
    'receiveBankNumber',
    'bankImage',
    'invoiceImage',
    'name',
    'contact',
    'contactMethod',
    'settlementMethod',
    'userId',
    'createdDate',
    'updatedDate',
  ],
  settlements: [
    'id',
    'type',
    'settlementType',
    'settled',
    'finalRate',
    'finalAmount',
    'costRate',
    'receivedCurrency',
    'sendCurrency',
    'userId',
    'externalId',
    'settledTime',
    'createdDate',
    'updatedDate',
  ],
};

const enumStatus = {
  users: {
    type: {
      ADMIN: 'ADMIN',
      CUSTOMER: 'CUSTOMER',
    },
  },
  advertisements: {
    type: {
      BUY: 'BUY',
      SELL: 'SELL',
    },
    status: {
      Listed: 'Listed',
      Expired: 'Expired',
    },
    crytocurrency: {
      USDT: 'USDT',
      ETH: 'ETH',
      BTC: 'BTC',
      XRP: 'XRP',
    },
  },
  buySellRecords: {
    type: {
      BUY: 'BUY',
      SELL: 'SELL',
    },
    status: {
      Processing: 'Processing',
      Matched: 'Matched',
      Cancelled: 'Cancelled',
    },
  },
  invoicePayments: {
    status: {
      Processing: 'Processing',
      Sent: 'Sent',
      Expired: 'Expired',
    },
    settlementMethod: {
      USDT: 'USDT',
      Cash: 'Cash',
    },
    contactMethod: {
      whatsapp: 'whatsapp',
      wechat: 'wechat',
      telegram: 'telegram',
      email: 'email',
    },
    type: {
      B2C: 'B2C',
      B2B: 'B2B',
    },
  },
  settlements: {
    type: {
      B2C: 'B2C',
      B2B: 'B2B',
      OTC: 'OTC',
      DIRECT: 'DIRECT',
    },
  },
};

const categories = [
  {
    id: 'users',
    name: 'User',
    isAdmin: false,
    icon: <PeopleIcon />,
  },
  {
    id: 'advertisements',
    name: 'Advertisement',
    isAdmin: false,
    icon: <AddShoppingCartIcon />,
  },
  {
    id: 'buySellRecords',
    name: 'Buy Sell Record',
    isAdmin: false,
    icon: <ListIcon />,
  },
  {
    id: 'invoicePayments',
    name: 'Invoice Payment',
    isAdmin: false,
    icon: <PaymentIcon />,
  },
  {
    id: 'settlements',
    name: 'Settlement',
    isAdmin: false,
    icon: <ReceiptIcon />,
  },
  {
    id: 'config',
    name: 'GlobalConfig',
    isAdmin: true,
    icon: <SettingsIcon />,
  },
  {
    id: 'devOps',
    name: 'DevOps',
    isAdmin: true,
    icon: <DeveloperBoardIcon />,
  },
  {
    id: 'statistic',
    name: 'Statistic',
    isAdmin: true,
    icon: <EqualizerIcon />,
  },
];

const roles = {
  ADMIN: 'ADMIN',
  USER: 'USER',
};

const notifyCategories = [
  'advertisements',
  'buySellRecords',
  'invoicePayments',
];

const generateId = () => `${(Math.random() * 0xFFFFFF << 0).toString(16).padStart(8, '0')}`;

export {
  headers,
  tokenKey,
  roleKey,
  additionalFields,
  hiddenFields,
  singleImgFields,
  searchableObjects,
  disabledFields,
  onCreateFields,
  objectFields,
  booleanFields,
  dateFields,
  categories,
  roles,
  enumStatus,
  notifyCategories,
  generateId,
};
