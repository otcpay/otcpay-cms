const longColumn = (key, editable) => ({
  title: key,
  field: key,
  editable,
  cellStyle: {
    width: 150,
    minWidth: 150,
    maxWidth: 150,
    wordBreak: 'break-word',
  },
  headerStyle: {
    width: 150,
    minWidth: 150,
    maxWidth: 150,
  },
});

export default {
  longColumn,
};
