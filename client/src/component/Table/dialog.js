import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

import { toggle } from '../../redux/actions';
import { disabledFields, onCreateFields, booleanFields } from '../../util/common';
import { SET_ALERT, SET_LOADING } from '../../redux/actionTypes';

function DialogForm(props) {
  const {
    columns, existingData, handleDialogClose, isOpen, title, createData, useField,
    toggleLoading, toggleAlert, token, env,
  } = props;

  const emptyData = {};
  columns.forEach((column) => { emptyData[useField ? column.field : column.title] = ''; });
  const [data, setData] = useState(existingData || emptyData);

  useEffect(() => {
    const tempData = {};
    columns.forEach((column) => { tempData[useField ? column.field : column.title] = ''; });
    setData(tempData);
  }, [columns]);

  const onChangeFile = (event) => {
    event.stopPropagation();
    event.preventDefault();
    const file = event.target.files[0];

    toggleLoading(true, SET_LOADING);

    const formData = new FormData();
    formData.append('file', file);
    formData.append('type', 'banner');

    fetch(`${env}/v1/file`, {
      headers: { Authorization: `Bearer ${token}` },
      method: 'POST',
      body: formData,
    }).then((response) => response.json())
      .then((res) => {
        if (res && res.mediaLink) {
          setData((previousState) => ({ ...previousState, url: res.mediaLink }));
        } else {
          throw new Error('Invalid response, please try again later');
        }
      }).catch((e) => {
        toggleLoading(false, SET_LOADING);
        toggleAlert(e.message, SET_ALERT);
      });
  };

  const handleChange = (event, column) => {
    let val = event.target.value;
    if (booleanFields.includes(column)) {
      val = event.target.checked;
    }
    setData((previousState) => ({ ...previousState, [column]: val }));
  };

  const renderItem = (column) => {
    if (column.lookup) {
      const keys = Object.keys(column.lookup);
      return keys.map((option) => (
        <MenuItem
          disableAutoFocus
          id={option}
          key={option}
          value={option}
        >
          {option}
        </MenuItem>
      ));
    }

    return null;
  };

  return (
    <Dialog onClose={handleDialogClose} open={isOpen}>
      <DialogTitle>{`Add ${title}`}</DialogTitle>
      <Paper style={{ padding: '2em' }}>
        <Grid container spacing={3}>
          {columns.map((column) => (
            <Grid item xs={column.field === 'url' || column.field === 'hyperlink' ? 12 : 6}>
              {
                column.field === 'url'
                  && (
                    <Button
                      variant='contained'
                      component='label'
                    >
                      Upload File
                      <input
                        type='file'
                        onChange={onChangeFile}
                        hidden
                      />
                    </Button>
                  )
              }
              {
                column.type && column.type === 'boolean' ? (
                  <FormControlLabel
                    value={useField ? column.field : column.title}
                    label={useField ? column.field : column.title}
                    labelPlacement='start'
                    control={(
                      <Checkbox
                        checked={data[useField ? column.field : column.title]}
                        onChange={(event) => handleChange(event, useField ? column.field : column.title)}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                      />
                    )}
                  />
                ) : (
                  <TextField
                    id={column.title}
                    key={column.title}
                    disabled={disabledFields.includes(column.title) && !onCreateFields.includes(column.title)}
                    select={column.lookup !== undefined}
                    type={column.type ? 'number' : undefined}
                    label={useField ? column.field : column.title}
                    value={data[useField ? column.field : column.title]}
                    onChange={(event) => handleChange(event, useField ? column.field : column.title)}
                    fullWidth
                  >
                    {renderItem(column)}
                  </TextField>
                )
              }
            </Grid>
          ))}
        </Grid>
        <div style={{ marginTop: '3em', float: 'right' }}>
          <Button
            id='create-button'
            onClick={() => {
              createData(data);
            }}
          >
            Save
          </Button>
          <Button
            id='cancel-button'
            onClick={handleDialogClose}
          >
            Cancel
          </Button>
        </div>
      </Paper>
    </Dialog>
  );
}

const mapStateToProps = (state) => ({
  lightBox: state.general.lightBox,
  visibility: state.general.visibility,
});

const mapDispatchToProps = {
  toggleLoading: toggle,
  toggleAlert: toggle,
};
DialogForm.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  existingData: PropTypes.shape({}),
  env: PropTypes.string,
  title: PropTypes.string,
  token: PropTypes.string,
  handleDialogClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  createData: PropTypes.func.isRequired,
  useField: PropTypes.bool,
  toggleAlert: PropTypes.func.isRequired,
  toggleLoading: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(DialogForm);
