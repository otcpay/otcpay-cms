import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import QRCode from 'react-qr-code';
import ButtonBase from '@material-ui/core/ButtonBase';
import MaterialCard from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import { toggle } from '../../../redux/actions';
import { SET_LIGHTBOX } from '../../../redux/actionTypes';

function DetailCard(props) {
  const {
    title, image, detail, qrCode, toggleLightBox,
  } = props;

  const openLightBox = (event) => {
    if (image) {
      toggleLightBox({ isOpen: true, image: `${image}?alt=media`, qrCode }, SET_LIGHTBOX);
    }
  }

  return (
    <Grid item>
      <ButtonBase onClick={openLightBox}>
        <MaterialCard style={{ maxWidth: 300 }}>
          <CardContent>
            <Typography gutterBottom variant='p' component='p'>
              {title}
            </Typography>
          </CardContent>
          {
            image && (
              <CardMedia
                style={{ height: 120 }}
                image={`${image}?alt=media`}
                title='Detail'
              />
            )
          }
          {
            qrCode && (
              <QRCode value={qrCode} size={175} />
            )
          }
          {
            detail && (
              <CardContent>
                <Typography gutterBottom variant='p' component='p'>
                  {detail}
                </Typography>
              </CardContent>
            )
          }
        </MaterialCard>
      </ButtonBase>
    </Grid>
  );
}

const mapDispatchToProps = {
  toggleLightBox: toggle,
};

DetailCard.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string,
  detail: PropTypes.string,
  qrCode: PropTypes.string,
  toggleLightBox: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(DetailCard);
