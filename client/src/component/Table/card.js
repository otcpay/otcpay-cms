import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';

import { headers } from '../../util/common';
import { toggle } from '../../redux/actions';
import { SET_LIGHTBOX, SET_ALERT, SET_LOADING } from '../../redux/actionTypes';

function LightBoxCard(props) {
  const inputFile = React.useRef();

  const {
    onClick, image, toggleLightBox, toggleAlert, toggleLoading,
    refreshTable, defaultClick, env, token, id, title, field,
  } = props;

  const onButtonClick = () => {
    inputFile.current.click();
  };

  const openLightBox = () => toggleLightBox({ isOpen: true, image: `${image}?alt=media` }, SET_LIGHTBOX);

  const defaultUploadFile = (event) => {
    event.stopPropagation();
    event.preventDefault();
    const file = event.target.files[0];

    toggleLoading(true, SET_LOADING);

    const data = new FormData();
    data.append('file', file);

    fetch(`${env}/v1/file`, {
      headers: { Authorization: `Bearer ${token}` },
      method: 'POST',
      body: data,
    }).then((response) => response.json())
      .then((res) => {
        if (res && res.mediaLink) {
          const payload = {};
          payload[field] = res.mediaLink;
          fetch(`${env}/v1/${title}/${id}`, {
            headers: { ...headers, Authorization: `Bearer ${token}` },
            method: 'PUT',
            body: JSON.stringify(payload),
          }).then((response) => response.json())
            .then((fileRes) => {
              toggleLoading(false, SET_LOADING);
              if (fileRes.result) {
                toggleAlert('Upload succeeded', SET_ALERT);
                refreshTable();
              } else {
                throw new Error('Upload failed');
              }
            }).catch((e) => {
              toggleLoading(false, SET_LOADING);
              toggleAlert(e.message, SET_ALERT);
            });
        } else {
          throw new Error('Invalid response, please try again later');
        }
      }).catch((e) => {
        toggleLoading(false, SET_LOADING);
        toggleAlert(e.message, SET_ALERT);
      });
  };

  return (
    <Card>
      <input
        id={`upload-input-${id}`}
        type='file'
        ref={inputFile}
        onChange={defaultUploadFile}
        style={{ display: 'none' }}
      />
      <CardMedia
        style={{ height: '75%' }}
      >
        <img
          id={`img-${id}`}
          src={`${image}?alt=media`}
          alt='Not found'
          style={{ display: 'block', marginLeft: 'auto', marginRight: 'auto' }}
          onClick={openLightBox}
          width='100%'
          height='100%'
        />
      </CardMedia>
      <CardActions>
        <Button
          id={`button-edit-${id}`}
          style={{ width: '100%', fontSize: '12px', padding: 0 }}
          onClick={defaultClick ? onButtonClick : onClick}
        >
          Edit
        </Button>
      </CardActions>
    </Card>
  );
}

const mapDispatchToProps = {
  toggleLightBox: toggle,
  toggleLoading: toggle,
  toggleAlert: toggle,
};

LightBoxCard.propTypes = {
  defaultClick: PropTypes.bool,
  onClick: PropTypes.func,
  field: PropTypes.string,
  env: PropTypes.string,
  title: PropTypes.string,
  token: PropTypes.string,
  refreshTable: PropTypes.func,
  id: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  toggleAlert: PropTypes.func.isRequired,
  toggleLoading: PropTypes.func.isRequired,
  toggleLightBox: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(LightBoxCard);
