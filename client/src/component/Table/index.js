import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import MaterialTable from 'material-table';
import Lightbox from 'react-image-lightbox';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';

import 'react-image-lightbox/style.css';

import Card from './card';
import column from './column';
import Dialog from './dialog';
import DetailCard from './Detail/DetailCard';
import { toggle } from '../../redux/actions';
import { SET_LIGHTBOX, SET_ALERT } from '../../redux/actionTypes';
import {
  headers, searchableObjects, objectFields, singleImgFields, additionalFields,
  dateFields, disabledFields, booleanFields, hiddenFields, enumStatus,
  onCreateFields, notifyCategories,
} from '../../util/common';

import whatsappIcon from './img/whatsapp.jpg';
import wechatIcon from './img/wechat.png';
import telegramIcon from './img/telegram.png';

function DataTable(props) {
  const {
    token, title, env, toggleLightBox, lightBox, visibility, toggleAlert,
  } = props;

  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const tableRef = React.useRef();
  const refreshTable = () => tableRef.current && tableRef.current.onQueryChange();

  useEffect(refreshTable, [title]);

  // eslint-disable-next-line no-unused-vars
  const handleDialogClose = (event) => {
    setIsDialogOpen(false);
  };

  // eslint-disable-next-line no-unused-vars
  const handleDialogOpen = (event) => {
    setIsDialogOpen(true);
  };

  const hasEditPermission = () => {
    const item = visibility.filter((v) => v.includes(title));
    if (item && item.length > 0) return !item[0].includes('Viewable');

    return false;
  };

  const hasDeletePermission = () => {
    const item = visibility.filter((v) => v.includes(title));
    if (item && item.length > 0) return item[0].includes('All');

    return false;
  };

  const getNotificationBody = (id, status) => {
    if (title === 'advertisements') {
      return `Your OTC Advertisement [${id}] is ${status}`;
    }

    if (title === 'buySellRecords') {
      return `Your OTC Request [${id}] is ${status}`;
    }

    return `Your Invoice Payment [${id}] is ${status}`;
  };

  const keys = [...objectFields[title]];
  additionalFields[title].forEach((f) => keys.splice(f.index, 0, f.key));
  const columns = keys.map((key) => {
    if (key === 'password') {
      return {
        title: key,
        field: key,
        editable: 'never',
        render: (data) => '******',
      };
    }

    if (key === 'user info') {
      return {
        title: key,
        field: key,
        editable: 'never',
        render: (data) => (
          <table>
            <tr>
              <td>Account</td>
              <td>{data.account}</td>
            </tr>
            <tr>
              <td>Username</td>
              <td>{data.username}</td>
            </tr>
          </table>
        ),
      };
    }

    if (key === 'verification') {
      return {
        title: key,
        field: key,
        editable: 'never',
        render: (data) => (
          <table>
            <tbody>
              <tr>
                <td>ID</td>
                <td>{data.idProof ? <CheckIcon /> : <ClearIcon />}</td>
              </tr>
              <tr>
                <td>Address</td>
                <td>{data.addressProof ? <CheckIcon /> : <ClearIcon />}</td>
              </tr>
              <tr>
                <td>Wallet</td>
                <td>{data.wallets && data.wallets.length > 0 ? <CheckIcon /> : <ClearIcon />}</td>
              </tr>
            </tbody>
          </table>
        ),
      };
    }

    if (key === 'profit') {
      return {
        title: key,
        field: key,
        editable: 'never',
        render: (data) => {
          if (data.settlementType === 'Cash') {
            return `${data.receivedCurrency} ${((data.finalRate - data.costRate) * data.finalAmount).toFixed(3)}`;
          }

          if (data.settlementType === 'USDT') {
            return `${((data.finalRate - data.costRate) * data.finalAmount).toFixed(3)} USDT`;
          }

          if (data.settlementType === 'BUY') {
            return `${((data.finalRate - data.costRate) * data.finalAmount).toFixed(3)} ${data.receivedCurrency}`;
          }

          return `${data.receivedCurrency} ${((data.costRate - data.finalRate) * data.finalAmount).toFixed(3)}`;
        },
      };
    }

    if (key === 'profit (HKD)') {
      return {
        title: key,
        field: key,
        editable: 'never',
        render: (data) => (data.finalRate - data.costRate) * data.finalAmount,
      };
    }

    if (title === 'users' && key === 'contact') {
      return {
        title: key,
        field: key,
        editable: 'never',
        render: (data) => (
          <table>
            <tr>
              <td><img alt='whatsapp' src={whatsappIcon} width='20' height='20' /></td>
              <td>{data.whatsappId}</td>
            </tr>
            <tr>
              <td><img alt='wechat' src={wechatIcon} width='20' height='20' /></td>
              <td>{data.wechatId}</td>
            </tr>
            <tr>
              <td><img alt='telegram' src={telegramIcon} width='20' height='20' /></td>
              <td>{data.telegramId}</td>
            </tr>
          </table>
        ),
      };
    }

    if (hiddenFields[title].includes(key)) {
      return {
        title: key,
        field: key,
        hidden: true,
      };
    }

    if (disabledFields.includes(key)) {
      if (dateFields.includes(key)) {
        return {
          title: key,
          field: key,
          editable: 'never',
          render: (data) => {
            if (data[key]) {
              return new Date(parseInt(data[key], 10)).toLocaleString();
            }

            return '-';
          },
        };
      }

      return column.longColumn(key, 'never');
    }

    if (singleImgFields.includes(key)) {
      return {
        field: key,
        title: key,
        render: (rowData) => (
          <Card
            defaultClick
            image={rowData[key]}
            field={key}
            env={env}
            token={token}
            title={title}
            id={rowData.id}
            refreshTable={refreshTable}
          />
        ),
      };
    }

    if (key !== 'password') {
      if (booleanFields.includes(key)) {
        return { title: key, field: key, type: 'boolean' };
      }

      if (enumStatus[title][key]) {
        return { title: key, field: key, lookup: enumStatus[title][key] };
      }

      return { title: key, field: key };
    }

    return { title: key, field: key, editable: 'onAdd' };
  });

  const renderDetailPanel = (rowData) => (
    <Grid container spacing={4}>
      {
        rowData.idProof && (
          <DetailCard
            title='ID Proof'
            detail={rowData.idProof.detail}
            image={rowData.idProof.image}
          />
        )
      }
      {
        rowData.addressProof && (
          <DetailCard
            title='Address Proof'
            detail={rowData.addressProof.detail}
            image={rowData.addressProof.image}
          />
        )
      }
      {
        rowData.wallets && rowData.wallets.map((d) => (
          <DetailCard
            title={`${d.type} Wallet - ${d.name}`}
            qrCode={d.address}
          />
        ))
      }
    </Grid>
  )

  const getOnCreateColumn = () => {
    const nonExistFields = additionalFields[title].map((f) => f.key);
    return columns.filter((c) => !(
      nonExistFields.includes(c.title)
      || (disabledFields.includes(c.title) && !onCreateFields.includes(c.title))
    ));
  };

  const createData = (newData) => new Promise((resolve, reject) => {
    fetch(`${env}/v1/${title}`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'POST',
      body: JSON.stringify(newData),
    }).then((response) => {
      if (response.ok) {
        handleDialogClose();
        refreshTable();
        resolve();
      }
      reject(new Error(`Response with ${response.status}`));
    }).catch((error) => {
      handleDialogClose();
      reject(error);
    });
  });

  const updateData = (newData, oldData) => new Promise((resolve, reject) => {
    if (notifyCategories.includes(title) && newData.status !== oldData.status) {
      fetch(`${env}/v1/notification/user`, {
        headers: { ...headers, Authorization: `Bearer ${token}` },
        method: 'POST',
        body: JSON.stringify({
          userId: newData.userId,
          body: getNotificationBody(newData.id, newData.status),
        }),
      }).then((response) => {
        if (!response.ok) {
          console.log('Failed to send status notification', newData);
        }
      });
    }

    if (title === 'buySellRecords' && newData.settled && !oldData.settled) {
      fetch(`${env}/v1/settlements`, {
        headers: { ...headers, Authorization: `Bearer ${token}` },
        method: 'POST',
        body: JSON.stringify({
          costRate: '0',
          externalId: newData.id,
          finalAmount: newData.amount,
          finalRate: newData.settleRate,
          receivedCurrency: newData.type === 'BUY' ? newData.crytocurrency : newData.settleCurrency,
          sendCurrency: newData.type === 'BUY' ? newData.settleCurrency : newData.crytocurrency,
          settled: true,
          settledTime: new Date().getTime().toString(),
          type: newData.adId ? 'OTC' : 'DIRECT',
          settlementType: newData.type,
          userId: newData.userId,
        }),
      }).then((response) => {
        if (!response.ok) {
          toggleAlert('Failed to create settlement', SET_ALERT);
          console.log('Failed to create settlement', newData);
        }
      });
    }

    if (title === 'invoicePayments' && newData.status === 'Sent' && oldData.status === 'Processing') {
      fetch(`${env}/v1/settlements`, {
        headers: { ...headers, Authorization: `Bearer ${token}` },
        method: 'POST',
        body: JSON.stringify({
          costRate: '0',
          externalId: newData.id,
          finalAmount: newData.amountNeeded,
          finalRate: newData.referenceRate,
          receivedCurrency: newData.receivedCurrency,
          sendCurrency: newData.sendCurrency,
          settled: true,
          settledTime: new Date().getTime().toString(),
          type: 'B2C',
          settlementType: newData.settlementMethod,
          userId: newData.userId,
        }),
      }).then((response) => {
        if (!response.ok) {
          toggleAlert('Failed to create settlement', SET_ALERT);
          console.log('Failed to create settlement', newData);
        }
      });
    }

    fetch(`${env}/v1/${title}/cms/${oldData.id}`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'PUT',
      body: JSON.stringify(newData),
    }).then((response) => {
      if (response.ok) {
        resolve();
      }
      reject(new Error(`Response with ${response.status}`));
    }).catch((error) => reject(error));
  });

  const deleteData = (oldData) => new Promise((resolve, reject) => {
    fetch(`${env}/v1/${title}/${oldData.id}`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'DELETE',
    }).then((response) => {
      if (response.ok) {
        resolve();
      }
      reject(new Error(`Response with ${response.status}`));
    }).catch((error) => reject(error));
  });

  const queryData = (query) => new Promise((resolve, reject) => {
    let url = `${env}/v1/${title}?refresh=false&page=${query.page + 1}&size=${query.pageSize}&fields=${objectFields[title].join(',')}`;
    if (query.orderBy) {
      url += `&orderBy=${query.orderBy.title}&direction=${query.orderDirection}`;
    }
    if (query.search) {
      url += `&keywords=${query.search}`;
    }
    fetch(url, { headers: { ...headers, Authorization: `Bearer ${token}` } })
      .then((response) => response.json())
      .then((result) => resolve({
        data: result.data,
        page: result.page - 1,
        totalCount: result.totalCount,
      }))
      .catch((error) => reject(error));
  });

  return (
    <Fragment>
      <MaterialTable
        title={title.toUpperCase()}
        tableRef={tableRef}
        columns={columns}
        options={{
          pageSize: 10,
          search: searchableObjects.includes(title),
          rowStyle: {
            fontSize: 12,
            padding: 0,
          },
        }}
        data={queryData}
        detailPanel={renderDetailPanel}
        actions={[
          {
            icon: 'refresh',
            tooltip: 'Refresh Data',
            isFreeAction: true,
            onClick: refreshTable,
          },
          {
            icon: 'add',
            tooltip: 'Add Data',
            isFreeAction: true,
            disabled: !hasEditPermission(),
            onClick: handleDialogOpen,
          },
        ]}
        editable={{
          isEditable: (rowData) => hasEditPermission(), // eslint-disable-line no-unused-vars
          isDeletable: (rowData) => hasDeletePermission(), // eslint-disable-line no-unused-vars
          onRowUpdate: updateData,
          onRowDelete: deleteData,
        }}
      />
      <Dialog
        title={title}
        columns={getOnCreateColumn()}
        isOpen={isDialogOpen}
        createData={createData}
        handleDialogClose={handleDialogClose}
      />
      {
        lightBox.isOpen && (
          <Lightbox
            mainSrc={lightBox.image}
            onCloseRequest={() => toggleLightBox({ isOpen: false, image: '' }, SET_LIGHTBOX)}
          />
        )
      }
    </Fragment>
  );
}

const mapStateToProps = (state) => ({
  lightBox: state.general.lightBox,
  visibility: state.general.visibility,
});

const mapDispatchToProps = {
  toggleLightBox: toggle,
  toggleAlert: toggle,
};

DataTable.propTypes = {
  env: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  lightBox: PropTypes.shape({
    isOpen: PropTypes.bool.isRequired,
    image: PropTypes.string.isRequired,
  }),
  visibility: PropTypes.arrayOf(String).isRequired,
  toggleAlert: PropTypes.func.isRequired,
  toggleLightBox: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(DataTable);
