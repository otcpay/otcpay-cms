import React from 'react';
import PropTypes from 'prop-types';

function DevOps(props) {
  const { env } = props;
  return (
    <div
      id='devops-iframe-container'
      style={{ width: '100%', height: 'calc(100vh - 128px)' }}
    >
      <iframe
        style={{ width: '100%', height: '100%' }}
        title='devops-dasboard'
        src={`${env}/admin`}
        name='devops-iframe'
      />
    </div>
  );
}

DevOps.propTypes = {
  env: PropTypes.string.isRequired,
};

export default DevOps;
