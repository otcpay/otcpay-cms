import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import clsx from 'clsx';

import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Tooltip from '@material-ui/core/Tooltip';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';

import {
  tokenKey, roles, roleKey, categories,
} from '../../util/common';
import { SET_LOGGEDIN, SET_ITEM, SET_ALERT } from '../../redux/actionTypes';
import Table from '../Table';
import Devops from '../DevOps';
import Chart from '../Chart';
import GlobalConfig from '../GlobalConfig';
import Copyright from '../Copyright';

import useStyles from './style';

function Dashboard(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(true);

  const token = localStorage.getItem(tokenKey);
  const role = localStorage.getItem(roleKey);
  const startIcon = role === roles.ADMIN ? <SupervisorAccountIcon /> : <AccountCircle />;
  const toolTip = role;

  const {
    selectedItem, toggleItem, toggleAlert, toggleActive, env, visibility,
  } = props;

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const logout = () => {
    toggleActive(false, SET_LOGGEDIN);
    toggleAlert('You have Logged Out', SET_ALERT);
    localStorage.removeItem(tokenKey);
  };

  const hasVisibility = (tabId) => {
    let visible = false;
    for (const tab of visibility) {
      if (tab.includes(tabId)) {
        visible = true;
        break;
      }
    }

    return visible;
  };

  const innerView = () => {
    if (selectedItem === 'devOps') {
      return (
        <Devops env={env} />
      );
    }

    if (selectedItem === 'config') {
      return (
        <GlobalConfig
          env={env}
          token={token}
          title={selectedItem}
        />
      );
    }

    if (selectedItem === 'statistic') {
      return (
        <Chart
          env={env}
          token={token}
          toggleAlert={toggleAlert}
        />
      );
    }

    return (
      <Table
        env={env}
        token={token}
        title={selectedItem}
      />
    );
  };

  const filteredCategories = categories.filter((c) => hasVisibility(c.id));

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position='absolute'
        className={clsx(classes.appBar, open && classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge='start'
            color='inherit'
            aria-label='open drawer'
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            component='h1'
            variant='h6'
            color='inherit'
            noWrap
            className={classes.title}
          >
            Dashboard
          </Typography>
          <Tooltip title={toolTip}>
            <Button
              style={{ color: 'white' }}
              onClick={logout}
              startIcon={startIcon}
            >
              Logout
            </Button>
          </Tooltip>
        </Toolbar>
      </AppBar>
      <Drawer
        variant='permanent'
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <ListItem button onClick={handleDrawerClose}>
            <ListItemIcon>
              <ChevronLeftIcon />
            </ListItemIcon>
            <ListItemText primary='Collapse' />
          </ListItem>
        </div>
        <Divider />
        <List>
          {filteredCategories.map((category) => (
            <ListItem
              key={category.id}
              selected={selectedItem === category.id}
              button
              onClick={() => toggleItem(category.id, SET_ITEM)}
            >
              <ListItemIcon>
                {category.icon}
              </ListItemIcon>
              <ListItemText primary={category.name} />
            </ListItem>
          ))}
        </List>
        <Divider />
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container
          maxWidth={false}
          className={clsx(classes.container, selectedItem === 'devOps' && classes.fullContainer)}
        >
          <Grid container spacing={3}>
            <Grid item xs={12}>
              {innerView()}
            </Grid>
          </Grid>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}

const mapStateToProps = (state) => ({
  selectedItem: state.general.selectedItem,
  visibility: state.general.visibility,
});

Dashboard.propTypes = {
  env: PropTypes.string.isRequired,
  selectedItem: PropTypes.string.isRequired,
  visibility: PropTypes.arrayOf(String).isRequired,
  toggleItem: PropTypes.func.isRequired,
  toggleAlert: PropTypes.func.isRequired,
  toggleActive: PropTypes.func.isRequired,
};

export default connect(mapStateToProps)(Dashboard);
