import React from 'react';

const rateColumn = [
  { title: 'From Currency', field: 'fromCurrency', editable: 'never' },
  { title: 'To Currency', field: 'toCurrency', editable: 'never' },
  { title: 'OTC board rate', field: 'otcBoardRate', type: 'numeric' },
  {
    title: 'External rate',
    field: 'externalRate',
    editable: 'never',
    render: (data) => (
      <table>
        {
          data.rates.map((d) => (
            <tr>
              <td>{d.name}</td>
              <td>
                {
                  (
                    data.fromCurrency === 'HKD'
                    && data.toCurrency === 'CNY'
                    && d.name !== 'Western Union'
                    && d.name !== 'MoneyGram'
                    && d.name !== 'TransferWise'
                    && d.name !== 'XE'
                    && d.name !== 'X-Rates'
                  ) || (
                    data.fromCurrency === 'CNY'
                    && data.toCurrency === 'HKD'
                    && d.name !== 'Western Union'
                    && d.name !== 'XE'
                    && d.name !== 'X-Rates'
                  )
                    ? 1 / d.rate
                    : d.rate
                }
              </td>
            </tr>
          ))
        }
      </table>
    ),
  },
  {
    title: 'Spreading',
    field: 'spreading',
    editable: 'never',
    render: (data) => {
      if (data.rates && data.rates.length > 0) {
        let refRate = data.rates[0].rate;
        if ((data.fromCurrency === 'HKD' && data.toCurrency === 'CNY') || (data.fromCurrency === 'CNY' && data.toCurrency === 'HKD')) {
          refRate = 1 / refRate;
        }
        return data.otcBoardRate - refRate;
      }

      return 0;
    },
  },
];

const bannerColumn = [
  { title: 'Url', field: 'url' },
  { title: 'HyperLink', field: 'hyperlink' },
  { title: 'Order', field: 'order' },
];

const otcColumn = [
  { title: 'Source Currency', field: 'sourceCurrency', editable: 'never' },
  { title: 'Target Currency', field: 'targetCurrency', editable: 'never' },
  { title: 'OTC board rate', field: 'otcBoardRate', type: 'numeric' },
  {
    title: 'Coin Mill rate', field: 'rate', type: 'numeric', editable: 'never',
  },
  {
    title: 'Spreading',
    field: 'spreading',
    editable: 'never',
    render: (data) => data.otcBoardRate - data.rate,
  },
  {
    title: 'Order', field: 'order', type: 'numeric', editable: 'never',
  },
  { title: 'Type', field: 'type', lookup: { BUY: 'BUY', SELL: 'SELL', editable: 'never' } },
];

export {
  rateColumn,
  bannerColumn,
  otcColumn,
};
