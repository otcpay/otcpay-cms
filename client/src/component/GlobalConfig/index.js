import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import MaterialTable from 'material-table';

import { rateColumn, bannerColumn, otcColumn } from './column';
import Dialog from '../Table/dialog';
import { headers, disabledFields } from '../../util/common';
import { toggle } from '../../redux/actions';
import { SET_LOADING } from '../../redux/actionTypes';

function TabPanel(props) {
  const {
    children, value, index, ...other
  } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`scrollable-prevent-tabpanel-${index}`}
      aria-labelledby={`scrollable-prevent-tab-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  value: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
};

function GlobalConfig(props) {
  const {
    token, title, env, visibility, toggleLoading,
  } = props;

  const tableRef = React.useRef();
  const tableRef2 = React.useRef();
  const tableRef3 = React.useRef();
  const refreshTable = () => tableRef.current && tableRef.current.onQueryChange();
  const refreshTable2 = () => tableRef2.current && tableRef2.current.onQueryChange();
  const refreshTable3 = () => tableRef3.current && tableRef3.current.onQueryChange();

  const [value, setValue] = useState(0);
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  let data = [];
  let otc = [];
  let banner = [];

  // eslint-disable-next-line no-unused-vars
  const handleDialogClose = (event) => {
    setIsDialogOpen(false);
  };

  // eslint-disable-next-line no-unused-vars
  const handleDialogOpen = (event) => {
    setIsDialogOpen(true);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const hasEditPermission = () => {
    const item = visibility.filter((v) => v.includes(title));
    if (item && item.length > 0) return !item[0].includes('Viewable');

    return false;
  };

  const getOnCreateColumn = () => {
    if (value === 0) return rateColumn.filter((c) => !disabledFields.includes(c.field));

    return bannerColumn.filter((c) => !disabledFields.includes(c.field));
  };

  const queryRateData = () => new Promise((resolve, reject) => {
    const url = `${env}/v1/fiat-rates/default?showExternal=true`;
    fetch(url, { headers: { ...headers, Authorization: `Bearer ${token}` } })
      .then((response) => response.json())
      .then((result) => {
        data = result;
        resolve({
          data: result,
          page: 0,
          totalCount: result.length,
        });
      })
      .catch((error) => reject(error));
  });

  const queryOtcRateData = () => new Promise((resolve, reject) => {
    const url = `${env}/v1/otc-rates`;
    fetch(url, { headers: { ...headers, Authorization: `Bearer ${token}` } })
      .then((response) => response.json())
      .then((result) => {
        otc = result;
        resolve({
          data: result.filter((r) => r.defaultRate),
          page: 0,
          totalCount: result.length,
        });
      })
      .catch((error) => reject(error));
  });

  const queryMappingData = () => new Promise((resolve, reject) => {
    const url = `${env}/system/mapping`;
    fetch(url, { headers: { ...headers, Authorization: `Bearer ${token}` } })
      .then((response) => response.json())
      .then((result) => {
        banner = result.hotpage;

        resolve({
          data: banner,
          page: 0,
          totalCount: banner.length,
        });
      })
      .catch((error) => reject(error));
  });

  const updateStatisticTime = () => new Promise((resolve, reject) => {
    fetch(`${env}/system/config/time`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'PUT',
      body: JSON.stringify({ lastUpdatedTime: new Date().getTime() }),
    }).then((response) => {
      if (!response.ok) console.log('Failed to update statistic time');
    }).catch((e) => console.log('Failed to update statistic time', e));
  });

  const onRateRowUpdate = (newData, oldData) => new Promise((resolve, reject) => {
    const index = data.findIndex((e) => e.fromCurrency === newData.fromCurrency && e.toCurrency === newData.toCurrency);
    data[index] = newData;

    toggleLoading(true, SET_LOADING);
    updateStatisticTime();

    fetch(`${env}/system/config/currency`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'PUT',
      body: JSON.stringify(data),
    }).then((response) => {
      toggleLoading(false, SET_LOADING);
      if (!response.ok) {
        reject(new Error('Failed to update'));
      }

      resolve();
    }).catch((e) => {
      toggleLoading(false, SET_LOADING);
      reject(new Error('Failed to update'));
    });
  });

  const onBannerRowUpdate = (newData, oldData) => new Promise((resolve, reject) => {
    const index = banner.findIndex((e) => e.url === oldData.url);
    banner[index] = newData;

    toggleLoading(true, SET_LOADING);

    fetch(`${env}/system/config/banner?target=hotpage`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'PUT',
      body: JSON.stringify(banner),
    }).then((response) => {
      toggleLoading(false, SET_LOADING);
      if (!response.ok) {
        reject(new Error('Failed to update'));
      }

      resolve();
    }).catch((e) => {
      toggleLoading(false, SET_LOADING);
      reject(new Error('Failed to update'));
    });
  });

  const onOtcRowUpdate = (newData, oldData) => new Promise((resolve, reject) => {
    const index = otc.findIndex(
      (e) => e.sourceCurrency === newData.sourceCurrency && e.targetCurrency === newData.targetCurrency && e.type === newData.type,
    );
    otc[index] = newData;

    toggleLoading(true, SET_LOADING);
    updateStatisticTime();

    fetch(`${env}/system/config/otc`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'PUT',
      body: JSON.stringify(otc),
    }).then((response) => {
      toggleLoading(false, SET_LOADING);
      if (!response.ok) {
        reject(new Error('Failed to update'));
      }

      resolve();
    }).catch((e) => {
      toggleLoading(false, SET_LOADING);
      reject(new Error('Failed to update'));
    });
  });

  const addRate = (newData) => new Promise((resolve, reject) => {
    updateStatisticTime();
  
    fetch(`${env}/system/config/currency`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'POST',
      body: JSON.stringify(newData),
    }).then((response) => {
      if (response.ok) {
        handleDialogClose();
        refreshTable();
        resolve();
      }
      reject(new Error(`Response with ${response.status}`));
    }).catch((error) => {
      handleDialogClose();
      reject(error);
    });
  });

  const addBanner = (newData) => new Promise((resolve, reject) => {
    fetch(`${env}/system/config/banner?target=hotpage`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
      method: 'POST',
      body: JSON.stringify(newData),
    }).then((response) => {
      if (response.ok) {
        handleDialogClose();
        refreshTable2();
        resolve();
      }
      reject(new Error(`Response with ${response.status}`));
    }).catch((error) => {
      handleDialogClose();
      reject(error);
    });
  });

  const addData = (newData) => {
    if (value === 0) {
      return addRate(newData);
    }

    return addBanner(newData);
  };

  return (
    <Fragment>
      <AppBar position='static'>
        <Tabs
          centered
          value={value}
          onChange={handleChange}
        >
          <Tab label='Daily Rate' />
          <Tab label='Banner' />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <MaterialTable
          title='FIAT'
          options={{
            paging: false,
            search: false,
            pageSize: 10,
            rowStyle: {
              fontSize: 12,
              padding: 0,
            },
          }}
          editable={{
            isEditable: (rowData) => hasEditPermission(), // eslint-disable-line no-unused-vars
            onRowUpdate: onRateRowUpdate,
          }}
          actions={[
            {
              icon: 'refresh',
              tooltip: 'Refresh Data',
              isFreeAction: true,
              onClick: refreshTable,
            },
            {
              icon: 'add',
              tooltip: 'Add Data',
              isFreeAction: true,
              disabled: !hasEditPermission(),
              onClick: handleDialogOpen,
            },
          ]}
          columns={rateColumn}
          tableRef={tableRef}
          data={queryRateData}
        />

        <div style={{ marginTop: 20, marginBottom: 20 }} />

        <MaterialTable
          title='OTC'
          options={{
            paging: false,
            search: false,
            pageSize: 10,
            rowStyle: {
              fontSize: 12,
              padding: 0,
            },
          }}
          editable={{
            isEditable: (rowData) => hasEditPermission(), // eslint-disable-line no-unused-vars
            onRowUpdate: onOtcRowUpdate,
          }}
          actions={[
            {
              icon: 'refresh',
              tooltip: 'Refresh Data',
              isFreeAction: true,
              disabled: !hasEditPermission(),
              onClick: refreshTable3,
            },
          ]}
          columns={otcColumn}
          tableRef={tableRef3}
          data={queryOtcRateData}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <MaterialTable
          title={title.toUpperCase()}
          options={{
            paging: false,
            search: false,
            pageSize: 10,
            rowStyle: {
              fontSize: 12,
              padding: 0,
            },
          }}
          editable={{
            isEditable: (rowData) => hasEditPermission(), // eslint-disable-line no-unused-vars
            onRowUpdate: onBannerRowUpdate,
          }}
          actions={[
            {
              icon: 'refresh',
              tooltip: 'Refresh Data',
              isFreeAction: true,
              onClick: refreshTable2,
            },
            {
              icon: 'add',
              tooltip: 'Add Data',
              isFreeAction: true,
              disabled: !hasEditPermission(),
              onClick: handleDialogOpen,
            },
          ]}
          columns={bannerColumn}
          tableRef={tableRef2}
          data={queryMappingData}
        />
      </TabPanel>
      <Dialog
        useField
        env={env}
        token={token}
        title={title}
        columns={getOnCreateColumn()}
        isOpen={isDialogOpen}
        createData={addData}
        handleDialogClose={handleDialogClose}
      />
    </Fragment>
  );
}

const mapStateToProps = (state) => ({
  visibility: state.general.visibility,
});

const mapDispatchToProps = {
  toggleLoading: toggle,
};

GlobalConfig.propTypes = {
  env: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  visibility: PropTypes.arrayOf(String).isRequired,
  toggleLoading: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(GlobalConfig);
