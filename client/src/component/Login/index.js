import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import {
  headers, tokenKey, roleKey,
} from '../../util/common';
import {
  SET_LOADING, SET_LOGGEDIN, SET_ALERT, SET_VISIBILITY,
} from '../../redux/actionTypes';
import Copyright from '../Copyright';

import useStyles from './style';

export default function Login(props) {
  const classes = useStyles();

  const {
    toggleActive, toggleAlert, toggleLoading, toggleVisibility, env,
  } = props;

  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');

  const onPasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const onUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const login = () => {
    toggleLoading(true, SET_LOADING);

    fetch(`${env}/authenticate`, {
      headers,
      method: 'POST',
      body: JSON.stringify({ username, password }),
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }

      throw new Error('Failed to login');
    }).then((data) => {
      toggleLoading(false, SET_LOADING);
      if (data.jwttoken && data.role) {
        if (data.role !== 'ADMIN' && data.role !== 'DEALER' && data.role !== 'OWNER') {
          throw new Error('Unauthorized access');
        }

        fetch(`${env}/authorization/groups/${data.role}`, {
          headers: { ...headers, Authorization: `Bearer ${data.jwttoken}` },
          method: 'GET',
        }).then((response) => {
          if (!response.ok) {
            throw new Error('Failed to get authorization group');
          }

          return response.json();
        }).then((authData) => {
          toggleVisibility(authData.permission, SET_VISIBILITY);
        });

        localStorage.setItem(tokenKey, data.jwttoken);
        localStorage.setItem(roleKey, data.role);

        toggleActive(true, SET_LOGGEDIN);
        toggleAlert(`Welcome ${username}`, SET_ALERT);
      } else {
        throw new Error('Unknown response from server, please try again later');
      }
    }).catch((e) => {
      toggleAlert(e.message, SET_ALERT);
      toggleLoading(false, SET_LOADING);
      toggleActive(false, SET_LOGGEDIN);
    });
  };

  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Sign in
        </Typography>
        <TextField
          variant='outlined'
          margin='normal'
          required
          fullWidth
          id='login-email'
          label='Email Address'
          name='email'
          autoComplete='email'
          autoFocus
          value={username}
          onChange={onUsernameChange}
        />
        <TextField
          variant='outlined'
          margin='normal'
          required
          fullWidth
          name='password'
          label='Password'
          type='password'
          id='login-password'
          autoComplete='current-password'
          value={password}
          onChange={onPasswordChange}
        />

        <Button
          id='login-submit'
          fullWidth
          variant='contained'
          color='primary'
          className={classes.submit}
          onClick={login}
        >
          Sign In
        </Button>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

Login.propTypes = {
  env: PropTypes.string.isRequired,
  toggleActive: PropTypes.func.isRequired,
  toggleAlert: PropTypes.func.isRequired,
  toggleLoading: PropTypes.func.isRequired,
  toggleVisibility: PropTypes.func.isRequired,
};
