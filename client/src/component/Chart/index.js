import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  BarChart,
  Tooltip,
  XAxis,
  YAxis,
  Legend,
  ResponsiveContainer,
  CartesianGrid,
  Bar,
  PieChart,
  Pie,
  Cell,
} from 'recharts';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { headers } from '../../util/common';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
const COLORS2 = ['#8884d8', '#82ca9d'];
export default function Chart(props) {
  const { token, toggleAlert, env } = props;

  const [data, setData] = useState([]);
  const [detailData, setDetailData] = useState({
    settlements: [],
    adCurrency: [],
    adType: [],
    otcCurrency: [],
    otcType: [],
  });

  const transformData = (payload) => {
    const transformedData = [];
    const keys = Object.keys(payload);
    keys.forEach((key) => {
      transformedData.push({
        name: key,
        today: payload[key].today,
        lastWeek: payload[key].lastWeek,
        lastTwoWeek: payload[key].lastTwoWeek,
        lastFourWeek: payload[key].lastFourWeek,
        lastMonth: payload[key].lastMonth,
        lastTwoMonth: payload[key].lastTwoMonth,
        lastThreeMonth: payload[key].lastThreeMonth,
        lastHalfYear: payload[key].lastHalfYear,
        absToday: payload[key].absToday,
        yesterday: payload[key].yesterday,
        twoDaysBefore: payload[key].twoDaysBefore,
        threeDaysBefore: payload[key].threeDaysBefore,
        fourDaysBefore: payload[key].fourDaysBefore,
        fiveDaysBefore: payload[key].fiveDaysBefore,
        sixDaysBefore: payload[key].sixDaysBefore,
        absWeek: payload[key].absWeek,
        absMonth: payload[key].absMonth,
        total: payload[key].total,
      });
    });
    return transformedData;
  };

  const transformToPieData = (oldData) => {
    const transformedData = [];
    const keys = Object.keys(oldData);
    keys.forEach((key) => {
      transformedData.push({
        name: key,
        value: oldData[key],
      });
    });
    return transformedData;
  };

  const transformDetailData = (payload) => {
    const transformedData = {
      settlements: transformToPieData(payload.Settlements),
      adCurrency: transformToPieData(payload.Advertisements.currency),
      adType: transformToPieData(payload.Advertisements.type),
      otcCurrency: transformToPieData(payload.BuySellRecords.currency),
      otcType: transformToPieData(payload.BuySellRecords.type),
    };
    return transformedData;
  };

  const getWeekDayName = (n) => {
    let e = new Date();
    e = new Date(e.getFullYear(), e.getMonth(), e.getDate() - n);
    return e.toLocaleDateString('en-HK', { weekday: 'long' });  
  }

  useEffect(() => {
    fetch(`${env}/system/info`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
    }).then((response) => response.json())
      .then((json) => setData(transformData(json)))
      .catch((error) => toggleAlert(error.message));

    fetch(`${env}/system/detail`, {
      headers: { ...headers, Authorization: `Bearer ${token}` },
    }).then((response) => response.json())
      .then((json) => setDetailData(transformDetailData(json)))
      .catch((error) => toggleAlert(error.message));
  }, []);

  return (
    <Fragment>
      <Typography
        id='core-title'
        component='h1'
        variant='h5'
      >
        Core data historical statistic (relative)
      </Typography>
      <ResponsiveContainer
        id='core-graph-container'
        width='100%'
        height={400}
      >
        <BarChart
          data={data}
        >
          <CartesianGrid strokeDasharray='3 3' />
          <XAxis dataKey='name' />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey='today' fill='#82ca9d' label={{ position: 'top' }} name='Last 24 Hrs' />
          <Bar dataKey='lastWeek' fill='#413ea0' label={{ position: 'top' }} name='Last week' />
          <Bar dataKey='lastTwoWeek' fill='#ff6700' label={{ position: 'top' }} name='Last two weeks' />
          <Bar dataKey='lastFourWeek' fill='#ffce30' label={{ position: 'top' }} name='Last four weeks' />
          <Bar dataKey='lastMonth' fill='#158fad' label={{ position: 'top' }} name='Last month' />
          <Bar dataKey='lastTwoMonth' fill='#ccac93' label={{ position: 'top' }} name='Last two month' />
          <Bar dataKey='lastThreeMonth' fill='#eb96eb' label={{ position: 'top' }} name='Last three month' />
          <Bar dataKey='lastHalfYear' fill='#8884d8' label={{ position: 'top' }} name='Last half year' />
        </BarChart>
      </ResponsiveContainer>

      <br />
      <br />
      <br />
      <br />

      <Typography
        id='core-title'
        component='h1'
        variant='h5'
      >
        Core data historical statistic (absolute)
      </Typography>
      <ResponsiveContainer
        id='core-graph-container'
        width='100%'
        height={400}
      >
        <BarChart
          data={data}
        >
          <CartesianGrid strokeDasharray='3 3' />
          <XAxis dataKey='name' />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey='absToday' fill='#82ca9d' label={{ position: 'top' }} name='Today' />
          <Bar dataKey='yesterday' fill='#413ea0' label={{ position: 'top' }} name={getWeekDayName(1)} />
          <Bar dataKey='twoDaysBefore' fill='#ccac93' label={{ position: 'top' }} name={getWeekDayName(2)} />
          <Bar dataKey='threeDaysBefore' fill='#ff6700' label={{ position: 'top' }} name={getWeekDayName(3)} />
          <Bar dataKey='fourDaysBefore' fill='#ccac93' label={{ position: 'top' }} name={getWeekDayName(4)} />
          <Bar dataKey='fiveDaysBefore' fill='#ffce30' label={{ position: 'top' }} name={getWeekDayName(5)} />
          <Bar dataKey='sixDaysBefore' fill='#b8255f' label={{ position: 'top' }} name={getWeekDayName(6)} />
          <Bar dataKey='absWeek' fill='#ff8d85' label={{ position: 'top' }} name='This Week' />
          <Bar dataKey='absMonth' fill='#158fad' label={{ position: 'top' }} name='This Month' />
        </BarChart>
      </ResponsiveContainer>

      <br />
      <br />
      <br />
      <br />

      <Typography
        id='core-title'
        component='h1'
        variant='h5'
      >
        Core data distribution statistic
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <Typography
            id='ad-title'
            variant='h6'
          >
            Advertisement
          </Typography>
          <ResponsiveContainer
            width='100%'
            height={300}
          >
            <PieChart height={300}>
              <Pie
                data={detailData.adType}
                dataKey='value'
                cx='50%'
                cy='50%'
                outerRadius={60}
                fill='#8884d8'
              >
                {detailData.adType.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS2[index % COLORS2.length]} />
                ))}
              </Pie>
              <Pie
                data={detailData.adCurrency}
                dataKey='value'
                cx='50%'
                cy='50%'
                innerRadius={70}
                outerRadius={90}
                label
              >
                {detailData.adCurrency.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                ))}
              </Pie>
              <Tooltip />
              <Legend />
            </PieChart>
          </ResponsiveContainer>
        </Grid>

        <Grid item xs={4}>
          <Typography
            id='ad-title'
            variant='h6'
          >
            OTC Records
          </Typography>
          <ResponsiveContainer
            width='100%'
            height={300}
          >
            <PieChart height={300}>
              <Pie
                data={detailData.otcType}
                dataKey='value'
                cx='50%'
                cy='50%'
                outerRadius={60}
                fill='#8884d8'
              >
                {detailData.otcType.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS2[index % COLORS2.length]} />
                ))}
              </Pie>
              <Pie
                data={detailData.otcCurrency}
                dataKey='value'
                cx='50%'
                cy='50%'
                innerRadius={70}
                outerRadius={90}
                label
              >
                {detailData.otcCurrency.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                ))}
              </Pie>
              <Tooltip />
              <Legend />
            </PieChart>
          </ResponsiveContainer>
        </Grid>

        <Grid item xs={4}>
          <Typography
            id='ad-title'
            variant='h6'
          >
            Invoice Payment
          </Typography>
          <ResponsiveContainer
            width='100%'
            height={300}
          >
            <PieChart height={300}>
              <Pie
                data={detailData.settlements}
                dataKey='value'
                cx='50%'
                cy='50%'
                outerRadius={90}
                label
              >
                {detailData.settlements.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                ))}
              </Pie>
              <Tooltip />
              <Legend />
            </PieChart>
          </ResponsiveContainer>
        </Grid>
      </Grid>
    </Fragment>
  );
}

Chart.propTypes = {
  env: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired,
  toggleAlert: PropTypes.func.isRequired,
};
