import {
  SET_LOADING, SET_LOGGEDIN, SET_ALERT, SET_ITEM, SET_ENV, SET_LIGHTBOX, SET_VISIBILITY, SET_NOTIFICATION,
} from '../actionTypes';

const initialState = {
  loading: false,
  loggedIn: false,
  selectedItem: 'users',
  alert: '',
  notification: '',
  visibility: [],
  env: 'https://otcpay-fx-dev.herokuapp.com/api',
  lightBox: {
    isOpen: false,
    image: '',
    qrCode: '',
  },
};

const visibilityFilter = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING:
      return { ...state, loading: action.payload };
    case SET_LOGGEDIN:
      return { ...state, loggedIn: action.payload };
    case SET_ALERT:
      return { ...state, alert: action.payload };
    case SET_ITEM:
      return { ...state, selectedItem: action.payload };
    case SET_ENV:
      return { ...state, env: action.payload };
    case SET_LIGHTBOX:
      return { ...state, lightBox: action.payload };
    case SET_VISIBILITY:
      return { ...state, visibility: action.payload };
    case SET_NOTIFICATION:
      return { ...state, notification: action.payload };
    default: {
      return state;
    }
  }
};

export default visibilityFilter;
