import Enzyme, {
  configure, shallow, mount, render,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

React.useLayoutEffect = React.useEffect;

HTMLCanvasElement.prototype.getContext = () => {};

configure({ adapter: new Adapter() });
export { shallow, mount, render };
export default Enzyme;
