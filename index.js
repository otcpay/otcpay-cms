const WebSocket = require('ws');
const http = require('http');
const path = require('path');
const express = require('express');

const serverPort = process.env.PORT || 5000;
const app = express();
const server = http.createServer(app);
const websocketServer = new WebSocket.Server({ server });

websocketServer.on('connection', (webSocketClient) => {
  webSocketClient.send('ok');

  // heartbeat keep alive
  setInterval(() => webSocketClient.send('ok'), 5000);

  webSocketClient.on('message', (message) => {
    websocketServer.clients.forEach((client) => {
      client.send(message);
    });
  });
});

app.use(express.json());

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.post('/notification', (req, res) => {
  websocketServer.clients.forEach((client) => {
    client.send(req.body.message);
  });
  res.status(200).send(null);
});

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res) => {
  res.sendFile(path.join(`${__dirname}/client/build/index.html`));
});

server.listen(serverPort, () => {
  console.log(`Websocket server started on port ${serverPort}`);
});
